FROM node:14
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
# Copying my project to the image.
COPY . . 
EXPOSE 3000
# Running the app
CMD [ "npm", "start" ]