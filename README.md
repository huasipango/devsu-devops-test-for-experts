My test summary
=====
### 1. Task: Clone the repo and make the app work.
----------------------
- Running the App.

![Evidence](https://gitlab.com/huasipango/devsu-devops-test-for-experts/uploads/aeae8155cb7efe7521a1230d7bc32480/1._running_the_app.JPG?raw=true "App capture")

### 2. Task: Dockerize the application.
----------------------
1. Create a Dockerfile for the project.
```dockerfile
FROM node:14
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
# Copying my project to the image.
COPY . . 
EXPOSE 3000
# Running the app
CMD [ "npm", "start" ]
```

2. Create a .dockerignore.
```dockerfile
node_modules
```

3. Build the image with a name.
```bash
docker build . -t huasipango/hello-world-node
```

4. Checking the new image.
```bash
docker image ls
```

![Evidence](https://gitlab.com/huasipango/devsu-devops-test-for-experts/uploads/6d6f7cc869edaff128bc84865a6feeee/2.image_created.JPG?raw=true "App capture")
5. Run the container on my port 8080.
```bash
docker run -p 8080:3000 huasipango/hello-world-node
```
6. Results.

![Evidence](https://gitlab.com/huasipango/devsu-devops-test-for-experts/uploads/4ba980999c7ad59f17ed73f8774f6660/2.docker-running-the-app.JPG?raw=true "App capture")

![Evidence](https://gitlab.com/huasipango/devsu-devops-test-for-experts/uploads/10231f9d2e35f0d3e215e25771220bca/2.acceding_from_browser.JPG?raw=true "App capture")

### 3. Task: Implement a simple load balancer using nginx and docker-compose.
----------------------

1. Creating the docker-compose.yaml with all the definitions.
```yaml
version: "3"
services:
  load_balancer:
    build:
      context: ./nginx
      dockerfile: Dockerfile
    ports:
      - "5001:5001"
    depends_on:
      - first_node_app
      - second_node_app
  first_node_app:
    build:
      context: .
      dockerfile: Dockerfile
    container_name: first_node_app
    build: .
    command: npm start
    ports: 
      - "5001:3000"
  second_node_app:
    build:
      context: .
      dockerfile: Dockerfile
    container_name: second_node_app
    build: .
    command: npm start
    ports: 
      - "5002:3000"
```

2. Node app running.

![Evidence](https://gitlab.com/huasipango/devsu-devops-test-for-experts/uploads/988c9df459f667f541d941a93927d3fb/3.loadbalancer.JPG?raw=true "App capture")

### 4. Task: Create a CI integration for the Application.
----------------------

1. Creating and config .gitlab-ci.yml
2. Check the [generated Pipeline in Gitlab.](https://gitlab.com/huasipango/devsu-devops-test-for-experts/-/pipelines)



## Hello World Node App

A simple hello world application built in node and express.

## Installation

- Make sure you have [node.js](https://nodejs.org/en/download/) installed
- `npm install`

## Start Server

`npm start`

## Run tests

`npm test`

## Endpoints

- `/hello`: says `Hello World!`
- `/hello/:name`: says hello with the name you pass as parameter in the URL.
